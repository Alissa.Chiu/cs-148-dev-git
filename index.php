<!DOCTYPE html>
<html lang="en"><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <title>Lab 1: PHP MyAdmin</title>
        <meta charset="utf-8">
        <meta name="author" content="Alissa Chiu">
        <meta name="description" content="Lab 1: PHP MyAdmin.">
      <link rel="stylesheet"
      href="style.css"
      type="text/css"
      media="screen">
      <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
      <link rel="icon" href="favicon.ico" type="image/x-icon">
      <style>  
    p {  
      text-indent: 2.0em;  
      }  
      </style> 
    </head>

    <body id="index"> 
    <h1>Lab 1: PHP MyAdmin</h1>

    <figure>
        <img src="tblTrails.png" alt="tblTrails.png"/>
        <figcaption>tblTrails.png</figcaption>
    </figure>
    
    <figure>
        <img src="tblTags.png" alt="tblTags.png"/>
        <figcaption>tblTags.png</figcaption>
    </figure>
    
    <figure>
        <img src="tblTrailsTags.png" alt="tblTrailsTags.png"/>
        <figcaption>tblTrailsTags.png</figcaption>
    </figure>
    
    <figure>
        <img src="ERD.png" alt="ERD.png"/>
        <figcaption>ERD.png</figcaption>
    </figure>

</body></html>